#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np 
import pandas as pd 
import seaborn as sns
import matplotlib.pyplot as plt
import datetime as dt

# fonction de formatage des données
# data_raw : données brutes de multimètre
def format_data(data_raw):
    data=data_raw.drop(data_raw[data_raw.resistance.eq(9999.9)].index)
    data=data.reset_index(drop=True)
    data['date_time']=data.date+' '+data.heure
    data.date_time=pd.to_datetime(data.date_time)
    data.drop(columns=['date','heure'],inplace=True)
    data['val0'] = data.iloc[0,6]
    data['heure_new']=data.date_time-data.val0
    data['seconde']=data.heure_new.dt.total_seconds()
    data_clean = data.drop(columns=['date_time','val0','heure_new'])
    data_clean['heure']=data_clean.seconde/3600
    data_clean['minute']=data_clean.seconde/60
    data_clean=data_clean[['heure','minute','seconde','conso-Wh', 'conso-Ah', 'tension', 'intensite', 'puissance',
           'resistance']]
    
    # calcul des dérivées de la puissance par rapport aux secondes
    diff_puissance = data_clean.puissance.diff(2)
    diff_puissance = diff_puissance.drop(0)
    diff_puissance = pd.concat([diff_puissance,pd.Series([np.nan],name='puissance')]).reset_index(drop=True)
    
    diff_tension = data_clean.tension.diff(2)
    diff_tension = diff_tension.drop(0)
    diff_tension = pd.concat([diff_tension,pd.Series([np.nan],name='tension')]).reset_index(drop=True)
    
    diff_resistance = data_clean.resistance.diff(2)
    diff_resistance = diff_resistance.drop(0)
    diff_resistance = pd.concat([diff_resistance,pd.Series([np.nan],name='resistance')]).reset_index(drop=True)

    diff_seconde = data_clean.seconde.diff(2)
    diff_seconde = diff_seconde.drop(0)
    diff_seconde = pd.concat([diff_seconde,pd.Series([np.nan],name='seconde')]).reset_index(drop=True)

    res=pd.concat([diff_seconde,diff_puissance,diff_tension,diff_resistance],axis=1)

    res.loc[res.seconde.notnull()|res.puissance.notnull()|res.seconde.ne(0),'deriv_puissance']=res.puissance/res.seconde
    res.loc[res.seconde.notnull()|res.tension.notnull()|res.seconde.ne(0),'deriv_tension']=res.tension/res.seconde
    res.loc[res.seconde.notnull()|res.resistance.notnull()|res.seconde.ne(0),'deriv_resistance']=res.resistance/res.seconde
    
    return pd.concat([data_clean,res.deriv_puissance,res.deriv_tension,res.deriv_resistance],axis=1)


# In[4]:

# data_raw : données brutes
# formatted : True si données déjà formattées
def findEndPoints(data_raw,formatted=False):
    if not formatted: # Si les données ne sont pas formatées, on utilise notre fonction format_data()
        data = format_data(data_raw)
    else : 
        data = data_raw.copy()
        
    id_max1 = data.deriv_puissance.idxmin() # Récupération de l'index de la valeur minimale de dérivée
    max1 = data["heure"][id_max1] # Heure de l'index de dérivée minimale de puissance
    
    # Suppression des valeurs autour du minimum de dérivé
    new_data = data.drop(range(id_max1-10,id_max1+10))
    
    id_max2 = new_data.deriv_puissance.idxmin() # Récupération de la 2ème valeur minimale de dérivée
    max2 = data["heure"][id_max2]
    
    if max1 > max2 : # Pour que les valeurs de minimums soient données dans l'ordre d'apparition
        return max2,max1
    return max1,max2


#
def cleanDataEnergimetre(data_raw):
    data = data_raw.copy()
    data.columns=['Sample','tension','intensite']
    data['puissance']=data.tension*data.intensite
    data['secondes']=0.100004*data.Sample
    data['heure']=data.secondes/3600
    data.drop(columns=['Sample'],inplace=True)
    
    data = data[['secondes','heure','intensite','tension','puissance']]
    
    return data

