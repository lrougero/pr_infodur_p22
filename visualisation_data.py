#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np 
import pandas as pd 
import seaborn as sns
import matplotlib.pyplot as plt
import datetime as dt
from utils import *
import matplotlib.ticker as ticker


# In[4]:


def plotPuissance(data_raw,formatted=False,derive=False,titre=None,save=False,nom_save=None):
    if not formatted:
        data = format_data(data_raw)
    else : 
        data = data_raw.copy()
    ax1=sns.lineplot(data=data,x='heure',y='puissance')
    ax1.set(ylabel='Puissance (W)')
    ax1.set(xlabel='Durée (h)')
    if titre is not None :
        ax1.set(title=titre)

    if derive :
        ax2 = ax1.twinx()
        sns.lineplot(data=data,x='heure',y='deriv_puissance',ax=ax2,color='r',alpha=0.4)
        ax1.figure.legend(['Puissance',"Dérivée"],title='Courbes',
                          loc='upper right', borderaxespad=0,bbox_to_anchor=(1.1, 0.8))
        ax2.set(ylabel='Dérivée (W/h)')
    
    if save==True :
        plt.savefig('../Visu/'+nom_save)
        

    plt.show()


# In[5]:


def plotIntensite(data_raw,titre=None,save=False,nom_save=None):
    data=format_data(data_raw)
    g1=sns.lineplot(data=data,x='heure',y='intensite')
    plt.legend(labels=["Intensité (A)"])
    g1.set(ylabel='Intensité')
    g1.set(xlabel='Heure')
    if titre is not None :
        g1.set(title=titre)
    g1.xaxis.set_major_locator(MultipleLocator(1))
    
    if save==True : 
        plt.savefig(nom_save)
        
    plt.show()


# In[6]:


def plotResistance(data_raw,titre=None,save=False,nom_save=None):
    data=format_data(data_raw)
    ax1=sns.lineplot(data=data,x='heure',y='resistance')
    ax1.set(ylabel='Résistance')
    ax1.set(xlabel='Heure')
    if titre is not None :
        ax1.set(title=titre)
    ax1.xaxis.set_major_locator(MultipleLocator(1))
    
    ax2 = ax1.twinx()
    sns.lineplot(data=data,x='heure',y='deriv_resistance',ax=ax2,color='r',alpha=0.4)
    
    plt.legend(labels=["Resistance (Ohm)","Dérivée (Ohm/s)"])
    ax2.set(ylabel='Dérivé')
    
    if save==True : 
        plt.savefig(nom_save)
        
    plt.show()


# In[7]:


def plotTension(data_raw,titre=None,save=False,nom_save=None):
    data=format_data(data_raw)
    ax1=sns.lineplot(data=data,x='heure',y='tension')
    ax1.set(ylabel='Tension')
    ax1.set(xlabel='Heure')
    if titre is not None :
        ax1.set(title=titre)
    ax2 = ax1.twinx()
    sns.lineplot(data=data,x='heure',y='deriv_tension',ax=ax2,color='r',alpha=0.4)
    
    plt.legend(labels=["Tension (V)","Dérivée (V/s)"])
    ax2.set(ylabel='Dérivé')
    
    if save :
        plt.savefig('~/Desktop/PR/Visu/'+nom_save)

    plt.show()


# In[8]:


def plotSuperpose(data_raw,titre=None,save=False,nom_save=None):
    data = format_data(data_raw)
    ax1=sns.lineplot(data=data,x='heure',y='puissance',legend=False)
    sns.lineplot(data=data,x='heure',y='intensite',ax=ax1)
    ax2 = ax1.twinx()
    sns.lineplot(data=data,x='heure',y='tension',legend=False,ax=ax2,color='g')
    ax1.set(ylabel='Puissance (W) / Intensité (A)')
    ax1.set(xlabel='Heure')
    ax2.set(ylabel="Tension (V)")
    ax1.figure.legend(['Puissance',"Intensité","Tension"],title='Courbes',bbox_to_anchor=(1.2, 0.8), loc='upper left', borderaxespad=0)
    if titre is not None : 
        ax1.set(title=titre)
    ax1.xaxis.set_major_locator(ticker.MultipleLocator(1))
    
    if save==True :
        plt.savefig(nom_save)
        
    plt.show()

