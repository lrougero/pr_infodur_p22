# PR_Infodur_P22

## Repo de la PR Infodur : Résilience des systèmes de stockage de données

### Exemple.ipynb : 
Démonstration de l'utilisation des différentes fonctions développées

### Utils.py : 
Fonctions utilitaires : 
- **format_data, cleanDataEnergimetre** Traitements de données brutes venant du multimètre ou de l'énergimètre 
- **findEndPoints**: Identification de points remarquables sur les données de recharge provenant du multimètre

### Visualisation_data.py : 
Création de graphiques permettant de visualiser les différents mesures réalisées par le multimètre UM25C ou par un énergimètre :
- **plotSuperpose** : courbes de puissance, tension et intensité
- **plotPuissance, plotIntensite, plotTension, plotResistance** : courbes de la grandeur mesurée avec sa dérivée

### Mesure_conso.py : 
Récupération de valeur de consommation :
- **getConso_Multimetre** : dernière valeur de consommation calculée par le multimètre
- **getConso_Rectangles** : calcul de la consommation énergétique en utilisant les points remarquables ou non






