#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np 
import pandas as pd 
import seaborn as sns
import matplotlib.pyplot as plt
import datetime as dt

from utils import *


# In[ ]:

# data_raw : données formattées ou non
# formatted : True si données déjà formattées, False sinon
def getConso_Rectangles(data_raw,n_point=None,formatted=False):
    if not formatted : 
        data = format_data(data_raw)
    else : 
        data = data_raw.copy()
    
    # on coupe les donnees à un des points caractéristiques si specifie
    if n_point is not None :
        if not formatted : 
            data= data.loc[data.heure.lt(findEndPoints(data_raw)[n_point-1])]
        else : 
            data= data.loc[data.heure.lt(findEndPoints(data_raw,True)[n_point-1])]
    
    time_delta = [0]
    puissance_mean = [0]

    for i in range(1,len(data)):
        # calcul de la difference entre deux mesures de temps 
        time_delta.append(data.heure[i]-data.heure[i-1]) 
        # on calcule la puissance moyenne pendant ce delta de temps 
        puissance_mean.append((data.puissance[i]+data.puissance[i-1])/2) 

    data["time_delta"] = time_delta
    data["puissance_mean"] = puissance_mean

    cols = ["time_delta", "puissance_mean"]
    data["time_delta*puiss_mean"] = data[cols].prod(axis=1)
    
    return data["time_delta*puiss_mean"].sum()


# In[ ]:

# data : données brutes provenant du multimètre
def getConso_Multimetre(data):
    return (data["conso-Wh"][len(data)-1])

